import React from 'react';

const Pagination = ({pages, handleClick}) => {
    return (
      <div className="App">
        {pages.map((item, index) => (
          <li
            // href="#/"
            key={index}
            value={item}
            style={{ margin: "10px", listStyle: "none", cursor: "pointer" }}
            onClick={handleClick}
          >
            {item}
          </li>
        ))}
      </div>
    );
  
}

export default Pagination;

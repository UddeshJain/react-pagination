import React, {useState, useEffect, useCallback} from 'react';
import PaginationComponent from "../../components/pagination/index"

const Pagination = () => {
    let total = 12;
    const [active, setActive] = useState(1);
    const [pages, setPages] = useState([1, 2, 3, 4, 5, "...", total])
    useEffect(() => {
      if(active <= 4) {
        setPages([1, 2, 3, 4, 5, "...", total])
      }
      if(active > 4 && active < total - 3) {
        setPages([1, "...", active - 1, active, active + 1, "...", total])
      }
      if(active > total - 4) {
        setPages([1, "...", total - 4, total - 3, total - 2, total - 1, total])
      }
    }, [active, total])
  
    const handleClick = useCallback((e) => {
      console.log(e.target.value);
      setActive(e.target.value)
    }, [])

    return (
        <div className="App">
        <PaginationComponent
            handleClick={handleClick}
            pages={pages}
        />
        </div>
    );
}

export default Pagination;

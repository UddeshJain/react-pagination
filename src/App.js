import React from 'react';
import Pagination from "./container/pagination/index"
import './App.css';

function App() {
  return (
    <div className="App">
      <Pagination />
    </div>
  );
}

export default App;
